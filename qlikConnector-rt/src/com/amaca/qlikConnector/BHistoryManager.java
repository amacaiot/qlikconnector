package com.amaca.qlikConnector;

import javax.baja.history.BBooleanTrendRecord;
import javax.baja.history.BHistoryService;
import javax.baja.history.BIHistory;
import javax.baja.history.BNumericTrendRecord;
import javax.baja.history.db.BHistoryDatabase;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import javax.baja.util.BTypeSpec;
import java.util.logging.Logger;

@NiagaraProperty(
        name = "syncTimeout",
        type = "baja:RelTime",
        defaultValue = "BRelTime.makeMinutes(5)"
)

@NiagaraProperty(
        name = "historyList",
        type = "BHistoryList",
        defaultValue = "new BHistoryList()"
)
@NiagaraAction(
        name = "refreshHistoryList"
)
@NiagaraAction(
        name = "removeAllHistoryDescriptors"
)
@NiagaraAction(
        name="startHistorySyncJob",
        flags = Flags.HIDDEN
)

@NiagaraType
/**
 * History Manager
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
public class BHistoryManager extends BAbstractService
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BHistoryManager(2343276819)1.0$ @*/
/* Generated Fri Apr 26 14:38:37 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "syncTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code syncTimeout} property.
   * @see #getSyncTimeout
   * @see #setSyncTimeout
   */
  public static final Property syncTimeout = newProperty(0, BRelTime.makeMinutes(5), null);
  
  /**
   * Get the {@code syncTimeout} property.
   * @see #syncTimeout
   */
  public BRelTime getSyncTimeout() { return (BRelTime)get(syncTimeout); }
  
  /**
   * Set the {@code syncTimeout} property.
   * @see #syncTimeout
   */
  public void setSyncTimeout(BRelTime v) { set(syncTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Property "historyList"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyList} property.
   * @see #getHistoryList
   * @see #setHistoryList
   */
  public static final Property historyList = newProperty(0, new BHistoryList(), null);
  
  /**
   * Get the {@code historyList} property.
   * @see #historyList
   */
  public BHistoryList getHistoryList() { return (BHistoryList)get(historyList); }
  
  /**
   * Set the {@code historyList} property.
   * @see #historyList
   */
  public void setHistoryList(BHistoryList v) { set(historyList, v, null); }

////////////////////////////////////////////////////////////////
// Action "refreshHistoryList"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code refreshHistoryList} action.
   * @see #refreshHistoryList()
   */
  public static final Action refreshHistoryList = newAction(0, null);
  
  /**
   * Invoke the {@code refreshHistoryList} action.
   * @see #refreshHistoryList
   */
  public void refreshHistoryList() { invoke(refreshHistoryList, null, null); }

////////////////////////////////////////////////////////////////
// Action "removeAllHistoryDescriptors"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code removeAllHistoryDescriptors} action.
   * @see #removeAllHistoryDescriptors()
   */
  public static final Action removeAllHistoryDescriptors = newAction(0, null);
  
  /**
   * Invoke the {@code removeAllHistoryDescriptors} action.
   * @see #removeAllHistoryDescriptors
   */
  public void removeAllHistoryDescriptors() { invoke(removeAllHistoryDescriptors, null, null); }

////////////////////////////////////////////////////////////////
// Action "startHistorySyncJob"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code startHistorySyncJob} action.
   * @see #startHistorySyncJob()
   */
  public static final Action startHistorySyncJob = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code startHistorySyncJob} action.
   * @see #startHistorySyncJob
   */
  public void startHistorySyncJob() { invoke(startHistorySyncJob, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BHistoryManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

  // empty constructor
  public BHistoryManager()
  {
    super();
  }

////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////

  public BQlikService getService()
  {
    return service;
  }

  public void setService(BQlikService service)
  {
    this.service = service;
  }

////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////

  // call the refresh histories descriptors
  public void doRefreshHistoryList()
  {
    updateHistoriesDescriptors();
  }

  // call the remove of all histories descriptors
  public void doRemoveAllHistoryDescriptors()
  {
    getHistoryList().removeAllHistoryDescriptors();
  }

  // submit the sync histories thread
  public void doStartHistorySyncJob(Context cx)
  {
    // get all descriptors
    BHistoryDescriptor[] allDescriptors = getHistoryList().getAllDescriptors();
    // get the number of all descriptors
    int numberOfDescriptors = allDescriptors.length;

    // start the job only if we have almost one descriptor
    if (numberOfDescriptors > 0)
    {
      // check if the job already exist, if not start it!
      if (historySyncjobRT == null && jobsInRunning() == false)
      {
        historySyncjobRT = new BReducedTrafficModeHistorySyncJob(this, getHistoryList().getAllDescriptors());
        BOrd jobOrd = historySyncjobRT.submit(cx);
      }
      else // if one job was already instantiated re-submit only if the previous one isn't in running
      {
        if (!historySyncjobRT.getJobState().isRunning() && jobsInRunning() == false) // check if the previous is finished
        {
          // start the new one
          historySyncjobRT = new BReducedTrafficModeHistorySyncJob(this, getHistoryList().getAllDescriptors());
          BOrd jobOrd = historySyncjobRT.submit(cx);
        }
      }
    }

  }

////////////////////////////////////////////////////////////////
// Thread management
////////////////////////////////////////////////////////////////

  // start a new ticket for thread
  private void startThreadClock()
  {
    // first, stop the previous
    stopThreadClock();

    // start the new one
    submitHistorySyncTicket = Clock.schedulePeriodically(this, getSyncTimeout(), startHistorySyncJob, null);
  }

  // stop current ticket and delete it.
  private void stopThreadClock()
  {
    if (submitHistorySyncTicket != null)
    {
      submitHistorySyncTicket.cancel();
      submitHistorySyncTicket = null;
    }
  }

////////////////////////////////////////////////////////////////
// History DB
////////////////////////////////////////////////////////////////

  // get all histories (numeric and boolean) available inside the station and create the descriptors
  private void updateHistoriesDescriptors()
  {
    // get History service
    BHistoryService service = (BHistoryService)Sys.getService(BHistoryService.TYPE);
    // get DB
    BHistoryDatabase db = service.getDatabase();
    // get the list of all available histories
    BIHistory[] histories = db.getHistories();

    // parse all histories
    for (int i = 0; i < histories.length; i++)
    {
      // identify only the numeric and boolean ones
      if(histories[i].getRecordType().equals(BTypeSpec.make(BNumericTrendRecord.TYPE)) || histories[i].getRecordType().equals(BTypeSpec.make(BBooleanTrendRecord.TYPE)))
      {
        logger.fine("Trying to add this history: " + histories[i].getId());
        // try to add the descriptor for current history
        getHistoryList().addHistoryDescriptor(histories[i]);
      }
    }

  }

 ///////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  public String getHistoryEndpoint()
  {
    return service.getUrl();
  }


  // check if some RTM jobs are in running.
  public boolean jobsInRunning()
  {
    if (historySyncjobRT1 != null)
      if (historySyncjobRT1.getProgress() < 100)
        return true;

    if (historySyncjobRT2 != null)
      if (historySyncjobRT2.getProgress() < 100)
        return true;

    if (historySyncjobRT3 != null)
      if (historySyncjobRT3.getProgress() < 100)
        return true;

    if (historySyncjobRT4 != null)
      if (historySyncjobRT4.getProgress() < 100)
        return true;

    if (historySyncjobRT5 != null)
      if (historySyncjobRT5.getProgress() < 100)
        return true;

    if (historySyncjobRT6 != null)
      if (historySyncjobRT6.getProgress() < 100)
        return true;

    return false;
  }

  public BPostRequestManager getPostRequestManager()
  {
    return service.getPostRequestsManager();
  }

////////////////////////////////////////////////////////////////
// service life-cycle
////////////////////////////////////////////////////////////////

  @Override
  public void descendantsStarted() throws Exception
  {
    super.descendantsStarted();

    // start the Clock for sync histories thread
    if (getEnabled())
      startThreadClock();
  }

  @Override
  public void changed(Property property, Context context)
  {
    super.changed(property, context);

    // check if we are in a running state
    if (!isRunning())return;
    if (!Sys.atSteadyState())return;

    // changed timeout? Update the thread ticket
    if (property == syncTimeout && getEnabled())
      startThreadClock();

    // service disabled -> stop thread ticket
    if (property == enabled)
      if (!getEnabled())
        stopThreadClock();
      else
        startThreadClock();
  }


  // get service type for service registration
  public Type[] getServiceTypes()
  {
    return serviceTypes;
  }

  private static Type[] serviceTypes = new Type[] { TYPE };

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("historyDatabase.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  private Clock.Ticket submitHistorySyncTicket = null;
  private BQlikService service = null;
  private BReducedTrafficModeHistorySyncJob historySyncjobRT = null;
  public BHistorySyncJobForRTM historySyncjobRT1 = null;
  public BHistorySyncJobForRTM historySyncjobRT2 = null;
  public BHistorySyncJobForRTM historySyncjobRT3 = null;
  public BHistorySyncJobForRTM historySyncjobRT4 = null;
  public BHistorySyncJobForRTM historySyncjobRT5 = null;
  public BHistorySyncJobForRTM historySyncjobRT6 = null;
  public static final Logger logger = Logger.getLogger("HistoryManager");
}
