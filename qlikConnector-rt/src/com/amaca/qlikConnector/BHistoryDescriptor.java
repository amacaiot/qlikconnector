package com.amaca.qlikConnector;

import com.tridium.history.db.BLocalDbHistory;
import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;

import javax.baja.history.BBooleanTrendRecord;
import javax.baja.history.BHistoryId;
import javax.baja.history.BNumericTrendRecord;
import javax.baja.history.BTrendRecord;
import javax.baja.naming.BOrd;
import javax.baja.naming.SlotPath;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import javax.baja.units.BUnit;
import javax.baja.util.BTypeSpec;
import java.util.StringTokenizer;

@NiagaraProperty(
        name = "historyId",
        type = "history:HistoryId",
        flags = Flags.READONLY,
        defaultValue = "BHistoryId.NULL"
)
@NiagaraProperty(
        name = "historyType",
        type = "baja:TypeSpec",
        flags = Flags.READONLY,
        defaultValue = "BTypeSpec.NULL"
)
@NiagaraProperty(
        name = "deviceName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "pointName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "lastSuccessfulSync",
        type = "baja:AbsTime",
        flags = Flags.READONLY,
        defaultValue = "BAbsTime.NULL"
)
@NiagaraProperty(
        name = "lastRecordSync",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "lastRecordSyncTimestamp",
        type = "baja:AbsTime",
        flags = Flags.READONLY,
        defaultValue = "BAbsTime.NULL"
)

@NiagaraType
/**
 * History descriptor
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
public class BHistoryDescriptor extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BHistoryDescriptor(3804422569)1.0$ @*/
/* Generated Fri Apr 26 09:23:44 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "historyId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyId} property.
   * @see #getHistoryId
   * @see #setHistoryId
   */
  public static final Property historyId = newProperty(Flags.READONLY, BHistoryId.NULL, null);
  
  /**
   * Get the {@code historyId} property.
   * @see #historyId
   */
  public BHistoryId getHistoryId() { return (BHistoryId)get(historyId); }
  
  /**
   * Set the {@code historyId} property.
   * @see #historyId
   */
  public void setHistoryId(BHistoryId v) { set(historyId, v, null); }

////////////////////////////////////////////////////////////////
// Property "historyType"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyType} property.
   * @see #getHistoryType
   * @see #setHistoryType
   */
  public static final Property historyType = newProperty(Flags.READONLY, BTypeSpec.NULL, null);
  
  /**
   * Get the {@code historyType} property.
   * @see #historyType
   */
  public BTypeSpec getHistoryType() { return (BTypeSpec)get(historyType); }
  
  /**
   * Set the {@code historyType} property.
   * @see #historyType
   */
  public void setHistoryType(BTypeSpec v) { set(historyType, v, null); }

////////////////////////////////////////////////////////////////
// Property "deviceName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code deviceName} property.
   * @see #getDeviceName
   * @see #setDeviceName
   */
  public static final Property deviceName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code deviceName} property.
   * @see #deviceName
   */
  public String getDeviceName() { return getString(deviceName); }
  
  /**
   * Set the {@code deviceName} property.
   * @see #deviceName
   */
  public void setDeviceName(String v) { setString(deviceName, v, null); }

////////////////////////////////////////////////////////////////
// Property "pointName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code pointName} property.
   * @see #getPointName
   * @see #setPointName
   */
  public static final Property pointName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code pointName} property.
   * @see #pointName
   */
  public String getPointName() { return getString(pointName); }
  
  /**
   * Set the {@code pointName} property.
   * @see #pointName
   */
  public void setPointName(String v) { setString(pointName, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastSuccessfulSync"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastSuccessfulSync} property.
   * @see #getLastSuccessfulSync
   * @see #setLastSuccessfulSync
   */
  public static final Property lastSuccessfulSync = newProperty(Flags.READONLY, BAbsTime.NULL, null);
  
  /**
   * Get the {@code lastSuccessfulSync} property.
   * @see #lastSuccessfulSync
   */
  public BAbsTime getLastSuccessfulSync() { return (BAbsTime)get(lastSuccessfulSync); }
  
  /**
   * Set the {@code lastSuccessfulSync} property.
   * @see #lastSuccessfulSync
   */
  public void setLastSuccessfulSync(BAbsTime v) { set(lastSuccessfulSync, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastRecordSync"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastRecordSync} property.
   * @see #getLastRecordSync
   * @see #setLastRecordSync
   */
  public static final Property lastRecordSync = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code lastRecordSync} property.
   * @see #lastRecordSync
   */
  public String getLastRecordSync() { return getString(lastRecordSync); }
  
  /**
   * Set the {@code lastRecordSync} property.
   * @see #lastRecordSync
   */
  public void setLastRecordSync(String v) { setString(lastRecordSync, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastRecordSyncTimestamp"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastRecordSyncTimestamp} property.
   * @see #getLastRecordSyncTimestamp
   * @see #setLastRecordSyncTimestamp
   */
  public static final Property lastRecordSyncTimestamp = newProperty(Flags.READONLY, BAbsTime.NULL, null);
  
  /**
   * Get the {@code lastRecordSyncTimestamp} property.
   * @see #lastRecordSyncTimestamp
   */
  public BAbsTime getLastRecordSyncTimestamp() { return (BAbsTime)get(lastRecordSyncTimestamp); }
  
  /**
   * Set the {@code lastRecordSyncTimestamp} property.
   * @see #lastRecordSyncTimestamp
   */
  public void setLastRecordSyncTimestamp(BAbsTime v) { set(lastRecordSyncTimestamp, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BHistoryDescriptor.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

  // empty constructor
  public BHistoryDescriptor()
  {
    super();
  }

////////////////////////////////////////////////////////////////
// Data
////////////////////////////////////////////////////////////////

  public JSONArray getNewDataForCloudInJSON()
  {
    // create a result array container
    JSONArray resultArray = new JSONArray();

    // resolve history
    BLocalDbHistory history = (BLocalDbHistory) BOrd.make("history:" + getHistoryId().toString()).get();
    // get a cursor
    Cursor cursor = history.cursor();
    // record limit (max 100 entity per request)
    int limit = 99;
    // get history facets
    BFacets facets = (BFacets) history.getConfig().get("valueFacets").newCopy();
    // get Measurement Unit
    String unitSymbol = ""; // as default empty
    try
    {
      BUnit unit = (BUnit) facets.get("units");
      unitSymbol = unit.getSymbol(); //TODO: gestione dei caratteri speciali.
    }
    catch (Exception e){};

    // read all record from start date
    while(cursor.next() && limit > 0)
    {
      // get the record
      BTrendRecord record = (BTrendRecord) cursor.get();
      // create a json object with the right structure with data of the current record
      JSONObject jsonRecord = new JSONObject();
      jsonRecord.put("device", getDeviceName());
      jsonRecord.put("timestamp", getTimeStampFormatted(record.getTimestamp()));
      jsonRecord.put("name", getPointName());
      if (getHistoryType().equals(BTypeSpec.make(BNumericTrendRecord.TYPE)))
        jsonRecord.put("value", "" + ((BNumericTrendRecord)record).getNumeric());
      else if (getHistoryType().equals(BTypeSpec.make(BBooleanTrendRecord.TYPE)))
        jsonRecord.put("value", "" + ((BBooleanTrendRecord)record).getBoolean());
      jsonRecord.put("measurementUnit", unitSymbol);
      jsonRecord.put("status", "" + record.getStatus().getBits());
      // put the record inside the result array
      resultArray.put(jsonRecord);
      // update the last records attributes
      lastRecord = jsonRecord.toString();
      lastRecordTimestamp = record.getTimestamp();
      // decrease the limit
      limit--;
    }

    return resultArray;
  }

  // return the string of the JSON array created (used in standard send mode)
  public String getNewDataForCloud()
  {
    return getNewDataForCloudInJSON().toString();
  }

  // update the "last data" slots
  public void confirmLastRecordAndLastSync()
  {
    setLastSuccessfulSync(BAbsTime.now());
    setLastRecordSync(lastRecord);
    setLastRecordSyncTimestamp(lastRecordTimestamp);
  }

////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  // get the device name based on ID structure: <deviceName>/<pointName>
  public void calculateDeviceAndPointName()
  {
    // get the historyId and create two token
    StringTokenizer stringTokenizer = new StringTokenizer(SlotPath.unescape(getHistoryId().encodeToString()), "/");
    // first part is the station name, ignore it.
    //stringTokenizer.nextToken();
    // second token is the device name. (station name)
    setDeviceName(stringTokenizer.nextToken());
    // last token is the point name
    setPointName(stringTokenizer.nextToken());
  }

  // check if the parameter timestamp is after the last successful send timestamp.
  private boolean isNew(BAbsTime time)
  {
    // if null the last success, all is new
    if (getLastRecordSyncTimestamp().isNull())
      return true;

    // check
    if( time.isAfter(getLastRecordSyncTimestamp()))
      return true;
    else
      return false;
  }

  // convert the niagara timestamp to requested format: "2018-03-14T11:46:07+02:00"
  private String getTimeStampFormatted(BAbsTime timestamp)
  {
    String result = "";

    // calculate and format the timezone
    String timezone = "";
    BRelTime tzoffset = BRelTime.make(timestamp.getTimeZoneOffset());

    if (timestamp.getTimeZoneOffset() > 0)
    {
      timezone += "+";
      timezone += mustBeDoubleDigit("" + tzoffset.getHours());
      timezone += ":00";
    }
    else if (timestamp.getTimeZoneOffset() < 0)
    {
      timezone += "-";
      timezone += mustBeDoubleDigit("" + (tzoffset.getHours()*(-1)));
      timezone += ":00";
    }
    else
      timezone += "+00:00";

    //generate the result string
    result += timestamp.getYear() + "-" + mustBeDoubleDigit("" + (timestamp.getMonth().getOrdinal()+1)) + "-" + mustBeDoubleDigit("" + timestamp.getDay());
    result +=  "T" + mustBeDoubleDigit("" + timestamp.getHour()) + ":" + mustBeDoubleDigit("" + timestamp.getMinute()) + ":" + mustBeDoubleDigit("" + timestamp.getSecond()) + timezone;


    return result;
  }

  // all numbers must have two digits at minimum
  private String mustBeDoubleDigit(String in)
  {
    if (in.length() == 1)
      return "0" + in;
    return in;
  }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon()
  {
    return BIcon.std("history.png");
  }
  private String lastRecord = null;
  private BAbsTime lastRecordTimestamp = null;
}
