package com.amaca.qlikConnector;

import com.tridium.util.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Restful post request manager
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
@NiagaraProperty(
        name = "requestTimeout",
        type = "baja:RelTime",
        defaultValue = "BRelTime.makeSeconds(10)"
)
@NiagaraType
public class BPostRequestManager extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BPostRequestManager(762693910)1.0$ @*/
/* Generated Sun Apr 14 16:51:21 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "requestTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code requestTimeout} property.
   * @see #getRequestTimeout
   * @see #setRequestTimeout
   */
  public static final Property requestTimeout = newProperty(0, BRelTime.makeSeconds(10), null);
  
  /**
   * Get the {@code requestTimeout} property.
   * @see #requestTimeout
   */
  public BRelTime getRequestTimeout() { return (BRelTime)get(requestTimeout); }
  
  /**
   * Set the {@code requestTimeout} property.
   * @see #requestTimeout
   */
  public void setRequestTimeout(BRelTime v) { set(requestTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BPostRequestManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BPostRequestManager()
    {
        super();
    }


 ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public int sendPostRequest(String url, String body) throws Exception
    {
      // define result status code
      int resultCode = 999;

      // create an HTTPClient instance
      CloseableHttpClient httpclient = HttpClients.createDefault();
      //log
      if (logger.isLoggable(Level.ALL))
      {
        logger.fine("Post request URL: " + url);
        logger.fine("Post request body: " + body);
      }

      try
      {
        // make the post request an set the url
        HttpPost httpPost = new HttpPost(url);
        // set header parameters
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        // set the body
        String customBody = "{\"payload_encoding\": \"string\"," +
                "\"properties\":{}," +
                "\"routing_key\":\"\"," +
                "\"payload\":\"" + StringEscapeUtils.escapeJava(body) + "\"}";

        logger.fine("Post request custom body: " + customBody);
        httpPost.setEntity(new StringEntity(customBody));
        // set the timeout of the request
        RequestConfig.Builder reqBuilder = RequestConfig.custom();
        reqBuilder.setSocketTimeout((int)getRequestTimeout().getMillis());
        httpPost.setConfig(reqBuilder.build());
        // execute the http post request and get the response
        CloseableHttpResponse response = httpclient.execute(httpPost);
        try
        {
          // update the result status code
          resultCode = response.getStatusLine().getStatusCode();
          if (logger.isLoggable(Level.FINE))
            logger.fine("Response status line: " + response.getStatusLine());
          HttpEntity entity = response.getEntity();
          EntityUtils.consume(entity);
        }
        catch (Exception e)
        {
          if (logger.isLoggable(Level.FINE))
            logger.fine("Exception: " + e.getMessage());
        }
        finally
        {
          response.close(); // always try to close the connection
        }
      }
      catch (Exception e)
      {
        if (logger.isLoggable(Level.FINE))
          logger.fine("Exception: " + e.getMessage());
        if (e instanceof ConnectTimeoutException)
          resultCode = 504;
      }
      finally
      {
        httpclient.close(); // always close the connection.
      }

      return resultCode;
    }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("gears.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
  public static final Logger logger = Logger.getLogger("PostRequestsManager");
}
