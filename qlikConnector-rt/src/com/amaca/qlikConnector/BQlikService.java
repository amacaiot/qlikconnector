package com.amaca.qlikConnector;

import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;

@NiagaraProperty(
        name = "url",
        type = "baja:String",
        defaultValue = "https://"
)

@NiagaraProperty(
        name = "postRequestsManager",
        type = "BPostRequestManager",
        defaultValue = "new BPostRequestManager()"
)

@NiagaraProperty(
        name = "HistoryManager",
        type = "BHistoryManager",
        defaultValue = "new BHistoryManager()"
)

@NiagaraType
/**
 * Qlik service
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
*/
public class BQlikService extends BAbstractService
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BQlikService(1239509464)1.0$ @*/
/* Generated Fri Apr 26 14:38:37 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "url"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code url} property.
   * @see #getUrl
   * @see #setUrl
   */
  public static final Property url = newProperty(0, "https://", null);
  
  /**
   * Get the {@code url} property.
   * @see #url
   */
  public String getUrl() { return getString(url); }
  
  /**
   * Set the {@code url} property.
   * @see #url
   */
  public void setUrl(String v) { setString(url, v, null); }

////////////////////////////////////////////////////////////////
// Property "postRequestsManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code postRequestsManager} property.
   * @see #getPostRequestsManager
   * @see #setPostRequestsManager
   */
  public static final Property postRequestsManager = newProperty(0, new BPostRequestManager(), null);
  
  /**
   * Get the {@code postRequestsManager} property.
   * @see #postRequestsManager
   */
  public BPostRequestManager getPostRequestsManager() { return (BPostRequestManager)get(postRequestsManager); }
  
  /**
   * Set the {@code postRequestsManager} property.
   * @see #postRequestsManager
   */
  public void setPostRequestsManager(BPostRequestManager v) { set(postRequestsManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "HistoryManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code HistoryManager} property.
   * @see #getHistoryManager
   * @see #setHistoryManager
   */
  public static final Property HistoryManager = newProperty(0, new BHistoryManager(), null);
  
  /**
   * Get the {@code HistoryManager} property.
   * @see #HistoryManager
   */
  public BHistoryManager getHistoryManager() { return (BHistoryManager)get(HistoryManager); }
  
  /**
   * Set the {@code HistoryManager} property.
   * @see #HistoryManager
   */
  public void setHistoryManager(BHistoryManager v) { set(HistoryManager, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BQlikService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Life cycle
////////////////////////////////////////////////////////////////
  @Override
  public void descendantsStarted() throws Exception
  {
    // call the superclass
    super.descendantsStarted();
    // set the this service as instance into history manager
    getHistoryManager().setService(this);
  }

  @Override
  public void changed(Property property, Context context)
  {
    super.changed(property, context);

    // check if we are in a running state
    if (!isRunning())return;
    if (!Sys.atSteadyState())return;

    // enable/disable sub-services accordingly
    if (property == enabled)
        getHistoryManager().setEnabled(getEnabled());

  }

////////////////////////////////////////////////////////////////
// Service compulsory stuff
////////////////////////////////////////////////////////////////

    public Type[] getServiceTypes()
    {
        return serviceTypes;
    }
    private static Type[] serviceTypes = new Type[] { TYPE };

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("cloud.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
}
