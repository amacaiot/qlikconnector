package com.amaca.qlikConnector;

import javax.baja.history.BHistoryId;
import javax.baja.history.BIHistory;
import javax.baja.naming.SlotPath;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.BComponent;
import javax.baja.sys.BIcon;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

@NiagaraType
/**
 * History list
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
public class BHistoryList extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BHistoryList(2979906276)1.0$ @*/
/* Generated Sun Apr 14 16:40:55 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BHistoryList.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BHistoryList()
    {
        super();
    }

////////////////////////////////////////////////////////////////
// Histories access methods
////////////////////////////////////////////////////////////////

    public void addHistoryDescriptor(BIHistory history)
    {
        // check if the history already have the descriptor, if no, continue.
        if (!checkIfExist(history.getId()))
        {
            // create a new instance of history descriptor
            BHistoryDescriptor historyDesc = new BHistoryDescriptor();
            // set all useful slots
            historyDesc.setHistoryId(history.getId());
            historyDesc.setHistoryType(history.getRecordType());
            historyDesc.calculateDeviceAndPointName();
            // add the new descriptor inside the list
            add(history.getId().getDeviceName() + SlotPath.escape("/") + history.getId().getHistoryName(), historyDesc);
        }
        else
        {
            if (logger.isLoggable(Level.FINE))
                logger.fine("History already have a descriptor: " + history.getId());
        }
    }


    // delete all history descriptors
    public void removeAllHistoryDescriptors()
    {
        removeAll();
    }

    // check if a specified history id already are managed and available in the list with their descriptor.
    public boolean checkIfExist(BHistoryId historyId)
    {
        // get all descriptor available in the list
        BHistoryDescriptor[] currentManagedHistories = getChildren(BHistoryDescriptor.class);

        // cycle all descriptors
        for (int i = 0; i < currentManagedHistories.length; i++)
        {
            // check if one have the same id of the parameter
            if (currentManagedHistories[i].getHistoryId().equals(historyId))
                return true; //found, return true.
        }
        // if not found, return false.
        return false;
    }

    // get all history descriptors
    public BHistoryDescriptor[] getAllDescriptors()
    {
        // get all descriptor available in the list
        BHistoryDescriptor[] currentManagedHistories = getChildren(BHistoryDescriptor.class);
        // return the array
        return currentManagedHistories;
    }


////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("historyGroup.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    public static final Logger logger = Logger.getLogger("HistoryList");
}
