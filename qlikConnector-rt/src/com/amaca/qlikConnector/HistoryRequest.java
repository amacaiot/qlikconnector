package com.amaca.qlikConnector;

import com.tridium.json.JSONArray;

import java.util.ArrayList;

/**
 * History request
 *
 * @author Amaca-5
 *         creation  15 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
public class HistoryRequest
{

    ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public HistoryRequest()
    {
        // create an empty json array
        resultArray = new JSONArray();

        // create the descriptor indexes and initialize it
        descriptorIndexes = new ArrayList<>();
    }

////////////////////////////////////////////////////////////////
// Add method
////////////////////////////////////////////////////////////////

    // check if there is enough space in the request for the input array values
    public boolean canAdd(JSONArray inputArray)
    {
        // calculate the sum from input array and current array size
        int sum = resultArray.length() + inputArray.length();

        // check if is >= the limit and return false if it is.
        if (sum >= limit)
            return false;
        else
            return true;
    }

    public boolean addValues(JSONArray inputArray, int descriptorIndex)
    {
        // check if there is space for the input array
        if (canAdd(inputArray))
        {
            try
            {
                // put all values of the input array inside the result array.
                for (int i = 0; i < inputArray.length(); i++)
                {
                    resultArray.put(inputArray.get(i));
                }

                //update the descriptor list
                descriptorIndexes.add(new Integer(descriptorIndex));

                // all ok, all added, return true
                return true;
            }
            catch (Exception e) // if occurs any exception return false
            {
                return false;
            }
        }
        else // return false, not enough space.
        {
            return false;
        }
    }


 ////////////////////////////////////////////////////////////////
// Get method
////////////////////////////////////////////////////////////////

    // get the string with all data
    public String getBodyRequest()
    {
        return resultArray.toString();
    }

 ////////////////////////////////////////////////////////////////
// Confirm method
////////////////////////////////////////////////////////////////

    // confirm all record sent
    public void confirmAll(BHistoryDescriptor[] allDescriptors)
    {
        // iterate all descriptors indexes and call the confirmation on descriptor.
        for (int i = 0; i < descriptorIndexes.size(); i++)
        {
            try
            {
                allDescriptors[descriptorIndexes.get(i).intValue()].confirmLastRecordAndLastSync();
            }
            catch (Exception e)
            {
                System.out.println("Exception in ConfirmAll on HistoryReqeust. Index: " + i + "on size: " + descriptorIndexes.size());
            }
        }
    }

////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

 ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    private int limit = 100;
    private ArrayList<Integer> descriptorIndexes = null;
    private JSONArray resultArray = null;
}
