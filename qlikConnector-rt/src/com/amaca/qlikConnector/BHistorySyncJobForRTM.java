package com.amaca.qlikConnector;

import javax.baja.job.BJobState;
import javax.baja.job.BSimpleJob;
import javax.baja.job.JobLogItem;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.ArrayList;
import java.util.logging.Logger;

@NiagaraType
/**
 * History sync for reduced traffic
 *
 * @author Amaca-5
 *         creation  8 Apr 19
 * @version $Revision: 1$ $Date: 08/04/19 12:00:00
 *
 */
public class BHistorySyncJobForRTM extends BSimpleJob
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.qlikConnector.BHistorySyncJobForRTM(2979906276)1.0$ @*/
/* Generated Mon Apr 15 17:35:00 CEST 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BHistorySyncJobForRTM.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

  // empty constructor
  public BHistorySyncJobForRTM()
  {
    // call superclass constructor
    super();
  }

  // main constructor
  public BHistorySyncJobForRTM(BHistoryManager hManager, BHistoryDescriptor[] allDescriptors, int start, int stop, ArrayList<HistoryRequest> hrequests)
  {
    // call superclass constructor
    super();
    // set the reference of history manager
    this.hManager = hManager;
    // set the descriptors to sync
    this.allDescriptors = allDescriptors;
    //set the start and stop descriptor
    this.startRequest = start;
    this.stopRequest = stop;
    // instantiate a new PostRequestManager based on the master one (timeout).
    this.postRequestsManager = new BPostRequestManager();
    this.postRequestsManager.setRequestTimeout( hManager.getPostRequestManager().getRequestTimeout());
    // get the endpoint
    this.endPoint = hManager.getHistoryEndpoint();
    // get the array list
    this.hrequests = hrequests;
  }

////////////////////////////////////////////////////////////////
// Job Cycle
////////////////////////////////////////////////////////////////

  @Override
  public void run(Context context) throws Exception
  {
    log().add(new JobLogItem(0, "Sync Job RTM started"));
    // counters
    int successCounter = 0;
    int failedCounter = 0;
    int skippedCounter = 0;

    // set the start time of the job
    BAbsTime startTime = BAbsTime.now();

    // calc the progressbar step and value
    double progressBarStep = (double)100 / (double)(stopRequest - startRequest);
    double progressBarValue = 0;

    // cycle all requests
    for (int i = startRequest; i < stopRequest; i++)
    {
      log().add(new JobLogItem(0, "Syncing request number: " + i));
      // get the new data formatted from the history
      String jsonBody = hrequests.get(i).getBodyRequest();
      String body = "";
      //The body contains history records separated by [{RECORD1},{RECORD2},{RECORD3}...]. Let's split them.
      String [] historyRecords = jsonBody.split("\\},\\{");
      for (int k = 0; k < historyRecords.length; k++) {
        //every history record is something like {"KEY1":"VALUE1","KEY2":"VALUE2"...} Let's split all those pairs.
        String [] pairs = historyRecords[k].split("\",\"");

        body = body.concat("<DATA>");
        try{ body = body.concat("<name>" + pairs[POINT_NAME].split("\":\"")[1] + "</name>"); }catch (ArrayIndexOutOfBoundsException e){ body = body.concat("<name></name>"); }
        try{ body = body.concat("<device>" + pairs[DEVICE_NAME].split("\":\"")[1] + "</device>"); }catch (ArrayIndexOutOfBoundsException e){ body = body.concat("<device></device>"); }
        try{ body = body.concat("<value>" + pairs[VALUE].split("\":\"")[1] + "</value>"); }catch (ArrayIndexOutOfBoundsException e){ body = body.concat("<value></value>"); }
        try{ body = body.concat("<measurementUnit>" + pairs[FACETS].split("\":\"")[1] + "</measurementUnit>"); }catch (ArrayIndexOutOfBoundsException e){ body = body.concat("<measurementUnit></measurementUnit>"); }
        try{ body = body.concat("<timestamp>" + pairs[TIMESTAMP].split("\":\"")[1] + "</timestamp>"); }catch (ArrayIndexOutOfBoundsException e){ body = body.concat("<timestamp></timestamp>"); }
        //Status is the last one so remove extra tail characters
        String status = "";
        try{ status = pairs[STATUS_BIT_MASK].split("\":\"")[1];} catch (ArrayIndexOutOfBoundsException e){ status = ""; }
        status = status.replace("\"","").replace("}","").replace("]","");
        body = body.concat("<status>" + status + "</status>");
        body = body.concat("</DATA>");
      }
      // send to post manager the request and get the result code
      int result = 0;
      try
      {
        if (body.length() > 10) // check if the body is consistent
          result = postRequestsManager.sendPostRequest(endPoint, body);
        else // if not, set 333: no new data.
          result = 333;
      }
      catch (Exception e){};

      // if result code is 200, confirm data sent correctly to the cloud and update timestamps
      if (result == 200) {
        hrequests.get(i).confirmAll(allDescriptors);
        log().add(new JobLogItem(0, ">> Sync OK"));
        // increment success counter
        successCounter++;
      }
      else if (result == 333)  //no new data
      {
        log().add(new JobLogItem(0, ">> No new data"));
        // increment skipped counter
        skippedCounter++;
      }
      else // sync error
      {
        log().add(new JobLogItem(0, ">> Syncing FAILED, error code: " + result));
        // increment failed counter
        failedCounter++;
      }

      // step up the progress
      progressBarValue += progressBarStep;
      progress((int)progressBarValue);
    }

    // calculate the execution time of the job
    BRelTime executionTime = startTime.delta(BAbsTime.now());
    // print the final message with statistics
    log().add(new JobLogItem(0, "Sync Job finished in: " + executionTime.toString()));
    log().add(new JobLogItem(0, ">> Number of Sync OK: " + successCounter));
    log().add(new JobLogItem(0, ">> Number of Sync FAILED: " + failedCounter));
    log().add(new JobLogItem(0, ">> Number of Sync SKIPPED: " + skippedCounter));
    // complete the job.
    progress(100);
    this.complete(BJobState.success);
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  public static final Logger logger = Logger.getLogger("HistoriesSyncJobRTM");
  private BHistoryManager hManager = null;
  private BHistoryDescriptor[] allDescriptors = null;
  private BPostRequestManager postRequestsManager = null;
  private String endPoint = "";
  private int startRequest = 0;
  private int stopRequest = 0;
  private ArrayList<HistoryRequest> hrequests = null;
  private final static int POINT_NAME = 0;
  private final static int DEVICE_NAME = 1;
  private final static int VALUE = 2;
  private final static int FACETS = 3;
  private final static int TIMESTAMP = 4;
  private final static int STATUS_BIT_MASK = 5;
}
